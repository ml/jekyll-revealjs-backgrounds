# Jekyll::Revealjs::Backgrounds

To modify the background of the current slide, jekyll-reveal.js also includes a simplification plugin:

    # Slide

    {% background white %}

    This slide has a white background

The the code originates from [Dennis Ploegers jekyll-revealjs](https://github.com/dploeger/jekyll-revealjs)
