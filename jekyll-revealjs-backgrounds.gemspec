# coding: utf-8
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "jekyll/revealjs/backgrounds/version"

Gem::Specification.new do |spec|
  spec.name          = "jekyll-revealjs-backgrounds"
  spec.version       = Jekyll::Revealjs::Backgrounds::VERSION
  spec.authors       = ["Marc Löchner"]
  spec.email         = ["marc.loechner@tu-dresden.de"]

  spec.summary       = "Modify the background of a reveal.js slide"
  spec.homepage      = "https://gitlab.vgiscience.de/ml/jekyll-revealjs-backgrounds"

end
