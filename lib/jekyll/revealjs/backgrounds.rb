require "jekyll/revealjs/backgrounds/version"

module Jekyll
  module Revealjs
    module Backgrounds
        class RenderBackground < Liquid::Tag

            def initialize(tag_name, text, tokens)
                super
                @text = text
            end

            def render(context)
                "<!-- .slide: data-background=\"#{@text}\" -->"
            end

        end
    end
  end
end

Liquid::Template.register_tag('background', Jekyll::Revealjs::Backgrounds::RenderBackground)
